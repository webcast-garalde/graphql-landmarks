const jwt = require("jsonwebtoken")
const APP_SECRET = 'project2-Landmark'

function getTokenPayload(token) {
    return jwt.verify(token, APP_SECRET)
}

function getUserId(req, authToken) {
    if (req) {
        // get value from authorization header
        const authHeader = req.headers.authorization
        if (authHeader) {
            const token = authHeader.replace("Bearer ", "")
            if (!token) {
                throw new Error("No token found.")
            }
            const { userId } = getTokenPayload(token)
            return Number(userId)
        }

    } else if (authToken) {
        const { userId } = getTokenPayload(token)
        return Number(userId)
    }

    throw new Error("Not authenticated.")

}


module.exports = {
    APP_SECRET,
    getUserId
}