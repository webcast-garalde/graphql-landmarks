const bcrypt = require("bcryptjs")
const jwt = require("jsonwebtoken")
const { APP_SECRET } = require("../auth")

async function signup(parent, args, context, info) {
    const password = await bcrypt.hash(args.password, 10)

    const user = await context.prisma.user.create({
        data: {
            ...args,
            password: password
        }
    })

    const token = jwt.sign({
        userId: `${user.id}`
    }, APP_SECRET)

    return {
        token,
        user
    }
}


async function login(parent, args, context, info) {
    const user = await context.prisma.user.findUnique({
        where: {
            email: args.email
        }
    })

    if (!user) {
        throw new Error("No such user found.")
    }

    const valid = await bcrypt.compare(args.password, user.password)
    if (!valid) {
        throw new Error("Invalid password.")
    }

    const token = jwt.sign({
        userId: `${user.id}`
    }, APP_SECRET)

    return {
        token,
        user
    }
}

async function addLandmark(parent, args, context, info) {
    const { userId } = context;
    if (!userId) {
        throw new Error("Please login to continue.")
    }

    const newLandmark = await context.prisma.landmark.create({
        data: {
            name: args.name,
            category: args.category,
            area: args.area,
            latitude: args.latitude,
            longitude: args.longitude
        }
    })

    return newLandmark
}

async function editLandmark(parent, args, context, info) {
    const { userId } = context;
    if (!userId) {
        throw new Error("Please login to continue.")
    }

    const updateLandmark = await context.prisma.landmark.update({
        where: {
            id: Number(args.landmarkId)
        },
        data: {
            name: args.name,
            category: args.category,
            area: args.area,
            latitude: args.latitude,
            longitude: args.longitude
        }
    })

    return updateLandmark
}

async function deleteLandmark(parent, args, context, info) {
    const { userId } = context;
    if (!userId) {
        throw new Error("Please login to continue.")
    }

    const landmark = await context.prisma.landmark.findUnique({
        where: {
            id: Number(args.landmarkId)
        }
    })

    if (Boolean(!landmark)) {
        throw new Error("Landmark does not exist.")
    }

    const removeLandmark = await context.prisma.landmark.delete({
        where: {
            id: Number(args.landmarkId)
        },
    })

    return removeLandmark
}

module.exports = {
    signup,
    login,
    addLandmark,
    editLandmark,
    deleteLandmark
}