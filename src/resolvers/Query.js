
function info() {
    return 'This is from resolvers folder Query.js'
}

async function get_landmarks(parents, args, context, info) {
    const { userId } = context
    if (!userId) {
        throw new Error("Please login to continue.")
    }

    // Assign args.condition to shorten the parameter
    const orAgrs = args.or_condition
    const andArgs = args.and_condition

    // Check if or_Condition is available in args.
    // If yes, create an OR condition using the filters from args
    // If or_Condition is not available, assign blank filter
    const orCondition = (args.or_condition)
        ? {
            OR: [
                { name: { contains: orAgrs.name } },
                { category: { contains: orAgrs.category } },
                { area: { contains: orAgrs.area } },
                { latitude: { equals: orAgrs.latitude } },
                { longitude: { equals: orAgrs.longitude } }
            ]
        }
        : {}

    // Check if and_Condition is available in args.
    // If yes, create an AND condition using the filters from args
    // If and_Condition is not available, assign blank filter    
    const andCondition = (args.and_condition)
        ? {
            AND: [
                { name: { contains: andArgs.name } },
                { category: { contains: andArgs.category } },
                { area: { contains: andArgs.area } },
                { latitude: { equals: andArgs.latitude } },
                { longitude: { equals: andArgs.longitude } }
            ]
        }
        : {}

    // Combine and_Condition and or_Condition filters
    // retrive records where (andCondition) and (orCondition)
    const where = {
        AND: [
            andCondition,
            orCondition
        ]
    }

    const landmarks = await context.prisma.landmark.findMany({
        where,
    })

    return landmarks
}

module.exports = {
    info,
    get_landmarks
}